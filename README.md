# KyivSmartCityHackathon Waste sort.

1. Please see Mockups:
    https://projects.invisionapp.com/project/18221414
    
2. Business workflow:
    https://whimsical.com/RU2rTarpz6GHjpq6Fn1qLY

3. Build project:
```javascript
    mnv clean install
```

4. Deploy project:
```javascript
   java -jar wastesort-0.0.1-SNAPSHOT.jar
``` 

API:

1. Get all types of Waste:<br />
**Request:**<br />
**GET:** base_url/waste-types<br />
Example: http://localhost:8080/waste-types<br />
**Response:**<br />
```javascript
[
    {
        "id": 1,
        "name": "Paper"
    },
    {
        "id": 2,
        "name": "Glass"
    }
]
```
2. A Сonsumer can get all new waste orders by types:<br />
**Request:**<br />
**GET:** base_url/orders/{list of waste types}<br />
Example: http://localhost:8080/orders/1,2<br />
**Response:**<br />
```javascript
[
    {
        "id": 3,
        "supplierId": 0,
        "consumerId": 0,
        "wasteUnits": [
            {
                "id": 5,
                "wasteType": {
                    "id": 2,
                    "name": "Glass"
                },
                "volume": 10,
                "weight": 10
            },
            {
                "id": 6,
                "wasteType": {
                    "id": 1,
                    "name": "Paper"
                },
                "volume": 220,
                "weight": 10
            }
        ],
        "location": "loc",
        "comment": "comm",
        "time": "time",
        "states": [
            {
                "id": 4,
                "name": "Open",
                "time": "2019-08-09T18:38:39.036+0000"
            }
        ]
    }
]
```

3. A Сonsumer can save own waste orders<br />
**Request:**<br />
**POST:** base_url/orders/{consumerId}/{wasteOrdersIds}<br />
Example: http://localhost:8080/orders/1/1,2<br />
**Response:**<br />
```javascript
{
    "id": 10,
    "supplierId": 2,
    "consumerId": null,
    "wasteUnits": [
        {
            "id": 12,
            "wasteType": {
                "id": 1,
                "name": "Paper"
            },
            "volume": 10,
            "weight": 10
        },
        {
            "id": 13,
            "wasteType": {
                "id": 2,
                "name": "Glass"
            },
            "volume": 220,
            "weight": 10
        }
    ],
    "location": "loc",
    "comment": "comm",
    "time": "time",
    "states": [
        {
            "id": 11,
            "name": "CREATED",
            "time": "2019-08-10T09:24:38.945+0000"
        }
    ]
}
```
4. A Supplier can create new waste order<br />
**Request:**<br />
**POST:** base_url/order<br />
Example: http://localhost:8080/orders<br />
Body:
```javascript
{
    "supplierId": 2,
    "consumerId": null,
    "units": [
        {
        	"typeId": 1,
            "volume": 10,
            "weight": 10
        },
        {
        	"typeId": 2,
            "volume": 220,
            "weight": 10
        }
    ],
    "location": "loc",
    "comment": "comm",
    "time": "time",
    "states": [
        {
            "name": "Open",
            "time": "2019-08-09T18:38:39.036+0000"
        }
    ]
}
```
**Response:**<br />
```javascript
{
    "id": 10,
    "supplierId": 2,
    "consumerId": null,
    "wasteUnits": [
        {
            "id": 12,
            "wasteType": {
                "id": 1,
                "name": "Paper"
            },
            "volume": 10,
            "weight": 10
        },
        {
            "id": 13,
            "wasteType": {
                "id": 2,
                "name": "Glass"
            },
            "volume": 220,
            "weight": 10
        }
    ],
    "location": "loc",
    "comment": "comm",
    "time": "time",
    "states": [
        {
            "id": 11,
            "name": "CREATED",
            "time": "2019-08-10T09:24:38.945+0000"
        }
    ]
}
```
5. A Supplier can own waste order<br />
**Request:**<br />
**GET:** base_url/orders/supplier/{supplierId}<br />
Example: http://localhost:8080/orders/supplier/2<br />
**Response:**<br />
```javascript
[
    {
        "id": 16,
        "supplierId": 2,
        "consumerId": null,
        "wasteUnits": [
            {
                "id": 18,
                "wasteType": {
                    "id": 1,
                    "name": "Paper"
                },
                "volume": 10,
                "weight": 10
            },
            {
                "id": 19,
                "wasteType": {
                    "id": 2,
                    "name": "Glass"
                },
                "volume": 220,
                "weight": 10
            }
        ],
        "location": "loc",
        "comment": "comm",
        "time": "time",
        "states": [
            {
                "id": 17,
                "name": "CREATED",
                "time": "2019-08-09T20:56:26.218+0000"
            }
        ]
    }
]
```






