import React, { useState, useEffect } from 'react';
import { Button } from 'react-bootstrap';
import { connect } from 'react-redux';

import { get, sendOrder } from '../../actions/appFlow';
import Map from '../../components/Map';

import './style.css';

function PickUp(props) {
  const [points, setPoints] = useState(null);
  const [selectedPoints, setSelectedPoints] = useState(null);

  function randomPoint(initLoc) {
    const i = Math.round(Math.random() * 3);

    return {
      loc: {
        lat: initLoc.lat + Math.random() * 0.1 - 0.03,
        lng: initLoc.lng + Math.random() * 0.1 - 0.03,
      },
      type:   ['Can', 'Paper', 'Plastic', 'Glass'][i],
      title:  ['Can', 'Paper', 'Plastic', 'Glass'][i],
      name:   ['Метал', 'Папір', 'Пластик', 'Скло'][i],
      icon:   `img/${['can', 'paper', 'plastic', 'glass'][i]}-loc.png`,
      weight: ['1-3 кг', '3-5 кг', '> 10 кг'][Math.round(Math.random() * 2)],
    };
  }

  function parseWeight(type, _weight) {
    let name;
    let weight;
    let icon;

    if (_weight === 1) weight = '1-3 кг';
    else if (_weight === 2) weight = '3-5 кг';
    else if (_weight === 3) weight = '> 10 кг';

    if (type === 3) {
      name = 'Пластик';
      icon = 'img/plastic-loc.png';
    } else if (type === 2) {
      name = 'Скло';
      icon = 'img/glass-loc.png';
    } else if (type === 1) {
      name = 'Папір';
      icon = 'img/paper-loc.png';
    } else if (type === 4) {
      name = 'Метал';
      icon = 'img/can-loc.png';
    }

    return { name, weight, icon };
  }


  useEffect(() => {
    if (points === null) {

      // if (props.units === null) {
      //   props.get();
      //   return;
      // }

      const array = [];
      const loc = {
        lat: 50.4851493,
        lng: 30.4721233,
      };

      for (let i = 0; i < 50; i++)
        array.push(randomPoint(loc));

      setPoints(array
        // props.units.map(unit => {
        //   const parsed = parseWeight(unit.type, unit.weight);
        //   const loc = { lat: unit.location.latitude, lng: unit.location.longitude };

        //   return {
        //     ...unit,
        //     ...parsed,
        //     loc,
        //   };
        // })

      );
    }
  });

  function order() {
    if (selectedPoints !== null) {
      console.log(selectedPoints);
      props.sendOrder(selectedPoints);
    }
  }

  console.log(selectedPoints);
  
  return (
    <div style={{ textAlign: 'center', height: '100vh' }}>
      <div style={{ height: 'calc(100vh - 50px)', display: 'grid', padding: '20px 20px 0px 20px' }}>
        <Map height={'100%'} points={points} isRout={true} onBuildRoute={setSelectedPoints}/>
      </div>
      <Button variant="secondary" style={{ width: 'calc(100% - 40px)', margin: '0px 20px 0px 20px' }} disabled={selectedPoints === null} onClick={order}>Забронювати</Button>
    </div>
  );
}

const mapStateToProps = ({ appFlow }) => ({
  units: appFlow.units,
});

const mapActionToProps = dispatch => ({
  get:       (...args) => dispatch(get(...args)),
  sendOrder: (...args) => dispatch(sendOrder(...args)),
});

export default connect(mapStateToProps, mapActionToProps)(PickUp);
