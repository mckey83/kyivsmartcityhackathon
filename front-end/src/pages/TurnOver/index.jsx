import React, { useState, useEffect } from 'react';

import Map from '../../components/Map';

import './style.css';

function TurnOver() {
  const [points, setPoints] = useState([]);

  function randomPoint(initLoc) {
    const i = Math.round(Math.random() * 3);

    return {
      loc: {
        lat: initLoc.lat + Math.random() * 0.1 - 0.05,
        lng: initLoc.lng + Math.random() * 0.1 - 0.05,
      },
      type:  ['Can', 'Paper', 'Plastic', 'Glass'][i],
      title: ['Can', 'Paper', 'Plastic', 'Glass'][i],
      name:  ['Метал', 'Папір', 'Пластик', 'Скло'][i],
      icon:  'img/waste-loc.png',
    };
  }

  useEffect(() => {
    if (!points.length) {
      const array = [];
      const loc = {
        lat: 50.4851493,
        lng: 30.4721233,
      };

      for (let i = 0; i < 50; i++)
        array.push(randomPoint(loc));

      setPoints(array);
    }
  });

  return (
    <div>
      <div style={{ height: '100vh', display: 'grid', padding: '20px' }}>
        <Map height={'100%'} points={points} isRout={true}/>
      </div>
    </div>
  );
}

export default TurnOver;
