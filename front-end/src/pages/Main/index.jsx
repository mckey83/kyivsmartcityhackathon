import React from 'react';
import { Button, Image } from 'react-bootstrap';

import './style.css';

function Main() {
  function clickButton(path) {
    return () => window.location.pathname = path;
  }

  return (
    <div style={{ height: '100vh' }}>
      <div className='header-div'>
        <Image src='img/transport-black@2x.png' style={{ height: '18px' }} />
        <label style={{ padding: '10px' }}>Транспортні послуги</label>
      </div>
      <div className='delimeter'></div>

      <div className='header-div'>
        <Image src='img/wallet@2x.png' style={{ height: '18px' }} />
        <label style={{ padding: '10px' }}>Сортування та здача вторсировини</label>
      </div>
      <div className='toolbar'>
        <Button variant="default" className='rectangle-btn' onClick={clickButton('/declare')}>
          <div className='oval'></div>
          <div className='i'>Я хочу вiддати</div>
        </Button>
        <Button variant="default" className='rectangle-btn' onClick={clickButton('/pick-up')}>
          <div className='oval' style={{ backgroundColor: '#DBBE0E' }}></div>
          <div className='i'>Я хочу забрати</div>
        </Button>
        <Button variant="default" className='rectangle-btn' onClick={clickButton('/turn-over')}>
          <div className='oval' style={{ backgroundColor: '#2355C0' }}></div>
          <div className='i'>Де здати?</div>
        </Button>
      </div>
      
      <div className='delimeter'></div>

      <div className='header-div'>
        <Image src='img/case@2x.png' style={{ height: '18px' }} />
        <label style={{ padding: '10px' }}>Електронна демократія</label>
      </div>
      <div className='delimeter'></div>
      
      <div className='header-div'>
        <Image src='img/shield-ok@2x.png' style={{ height: '18px' }} />
        <label style={{ padding: '10px' }}>Послуги МВС</label>
      </div>
      <div className='delimeter'></div>

      <div className='header-div'>
        <Image src='img/cross@2x.png' style={{ height: '18px' }} />
        <label style={{ padding: '10px' }}>Соціальний захист</label>
      </div>
      <div className='delimeter'></div>

      <div className='header-div'>
        <Image src='img/toy-horse@2x.png' style={{ height: '18px' }} />
        <label style={{ padding: '10px' }}>Послуги для сім’ї</label>
      </div>
      <div className='delimeter'></div>

      <div className='header-div'>
        <Image src='img/pet-black@2x.png' style={{ height: '12px' }} />
        <label style={{ padding: '10px' }}>Реєстр домашніх тварин</label>
      </div>
      <div className='delimeter'></div>
    
    </div>
  );
}

export default Main;
