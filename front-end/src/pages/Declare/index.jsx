import React, { useState } from 'react';
import { Button, Dropdown, Image } from 'react-bootstrap';
import { connect } from 'react-redux';

import { send } from '../../actions/appFlow';
import Map from '../../components/Map';

import './style.css';

function Declare(props) {
  const [plastic, setPlastic] = useState({ isActive: false });
  const [glass, setGlass] = useState({ isActive: false });
  const [paper, setPaper] = useState({ isActive: false });
  const [can, setCan] = useState({ isActive: false });
  const [loc, setLoc] = useState({});

  const [phoneNumber, setPhoneNumber] = useState();
  const [email, setEmail] = useState();
  const [comment, setComment] = useState();

  function activate(value, set) {
    return () => set({ isActive: !value.isActive });
  }

  function selectWeight(weight, garbage, set) {
    return () => {
      set({ ...garbage, weight });
    };
  }

  function inputChange(set) {
    return (e) => {
      const value = e.target.value;

      set(value);
    };
  }

  function parseWeight(weight) {
    if (weight === '1-3 кг') return 1;
    if (weight === '3-5 кг') return 2;
    if (weight === '> 10 кг') return 3;
  }

  function sendPropose() {
    const units = [];

    if (plastic.isActive) {
      units.push({ typeId: 3, weight: parseWeight(plastic.weight) });
    }
    if (paper.isActive) {
      units.push({ typeId: 1, weight: parseWeight(paper.weight) });
    }
    if (glass.isActive) {
      units.push({ typeId: 2, weight: parseWeight(glass.weight) });
    }
    if (can.isActive) {
      units.push({ typeId: 4, weight: parseWeight(can.weight) });
    }

    if (units.length) {
      props.send(loc, units, comment);
    }
  }

  return (
    <div>
      <div className='i-head'>
        <label>Я хочу вiддати</label>
      </div>

      <div style={{ padding: '20px' }}>
        <div style={{ display: 'inline-block', width: '100%' }}>
          <div className='garbage-block' style={{ float: 'left', width: '45%' }}>
            <Button variant="default" className={plastic.isActive ? 'rectangle-selected' : 'rectangle'} onClick={activate(plastic, setPlastic)}>
              <div className='oval' style={{ backgroundColor: '#4A90E2' }}>
                <Image src='img/plastic.png' className='my-image' />
              </div>
              <div className='garbage-label'>Пластик</div>
            </Button>

            <div className={plastic.isActive ? 'rectangle-small' : 'rectangle-small-innactive'}>
              <div className='weight-label'>Вага:</div>
              <Dropdown className='weight-input' drop="right" show={plastic.isActive ? undefined : false}>
                <Dropdown.Toggle variant="default" style={{ width: '100%', height: '100%' }}>
                  {plastic.weight || ''}
                </Dropdown.Toggle>

                <Dropdown.Menu>
                  <Dropdown.Item as="button" onClick={selectWeight('1-3 кг', plastic, setPlastic)}>1-3 кг</Dropdown.Item>
                  <Dropdown.Item as="button" onClick={selectWeight('5-10 кг', plastic, setPlastic)}>5-10 кг</Dropdown.Item>
                  <Dropdown.Item as="button" onClick={selectWeight('> 10 кг', plastic, setPlastic)}>{'> 10 кг'}</Dropdown.Item>
                </Dropdown.Menu>
              </Dropdown>
            </div>
          </div>
          
          <div className='garbage-block' style={{ float: 'right', width: '45%' }}>
            <Button variant="default" className={paper.isActive ? 'rectangle-selected' : 'rectangle'} onClick={activate(paper, setPaper)}>
              <div className='oval' style={{ backgroundColor: '#F8E71C' }}>
                <Image src='img/paper.png' className='my-image' />
              </div>
              <div className='garbage-label'>Папiр</div>
            </Button>

            <div className={paper.isActive ? 'rectangle-small' : 'rectangle-small-innactive'}>
              <div className='weight-label'>Вага:</div>
              <Dropdown className='weight-input' drop="right" show={paper.isActive ? undefined : false}>
                <Dropdown.Toggle variant="default" style={{ width: '100%', height: '100%' }}>
                  {paper.weight || ''}
                </Dropdown.Toggle>

                <Dropdown.Menu>
                  <Dropdown.Item as="button" onClick={selectWeight('1-3 кг', paper, setPaper)}>1-3 кг</Dropdown.Item>
                  <Dropdown.Item as="button" onClick={selectWeight('5-10 кг', paper, setPaper)}>5-10 кг</Dropdown.Item>
                  <Dropdown.Item as="button" onClick={selectWeight('> 10 кг', paper, setPaper)}>{'> 10 кг'}</Dropdown.Item>
                </Dropdown.Menu>
              </Dropdown>
            </div>
          </div>
        </div>
        
        <div style={{ display: 'inline-block', width: '100%' }}>
          <div className='garbage-block' style={{ float: 'left', width: '45%' }}>
            <Button variant="default" className={glass.isActive ? 'rectangle-selected' : 'rectangle'} onClick={activate(glass, setGlass)}>
              <div className='oval' style={{ backgroundColor: 'rgba(72,174,16,0.5)' }}>
                <Image src='img/glass.png' style={{ height: '100%' }} />
              </div>
              <div className='garbage-label'>Скло</div>
            </Button>

            <div className={glass.isActive ? 'rectangle-small' : 'rectangle-small-innactive'}>
              <div className='weight-label'>Вага:</div>
              <Dropdown className='weight-input' drop="right" show={glass.isActive ? undefined : false}>
                <Dropdown.Toggle variant="default" style={{ width: '100%', height: '100%' }}>
                  {glass.weight || ''}
                </Dropdown.Toggle>

                <Dropdown.Menu>
                  <Dropdown.Item as="button" onClick={selectWeight('1-3 кг', glass, setGlass)}>1-3 кг</Dropdown.Item>
                  <Dropdown.Item as="button" onClick={selectWeight('5-10 кг', glass, setGlass)}>5-10 кг</Dropdown.Item>
                  <Dropdown.Item as="button" onClick={selectWeight('> 10 кг', glass, setGlass)}>{'> 10 кг'}</Dropdown.Item>
                </Dropdown.Menu>
              </Dropdown>
            </div>
          </div>
          
          <div className='garbage-block' style={{ float: 'right', width: '45%' }}>
            <Button variant="default" className={can.isActive ? 'rectangle-selected' : 'rectangle'} onClick={activate(can, setCan)}>
              <div className='oval' style={{ backgroundColor: '#FF5468' }}>
                <Image src='img/can.png' className='my-image' />
              </div>
              <div className='garbage-label'>Метал</div>
            </Button>

            <div className={can.isActive ? 'rectangle-small' : 'rectangle-small-innactive'}>
              <div className='weight-label'>Вага:</div>
              <Dropdown className='weight-input' drop="right" show={can.isActive ? undefined : false}>
                <Dropdown.Toggle variant="default" style={{ width: '100%', height: '100%' }}>
                  {can.weight || ''}
                </Dropdown.Toggle>

                <Dropdown.Menu>
                  <Dropdown.Item as="button" onClick={selectWeight('1-3 кг', can, setCan)}>1-3 кг</Dropdown.Item>
                  <Dropdown.Item as="button" onClick={selectWeight('5-10 кг', can, setCan)}>5-10 кг</Dropdown.Item>
                  <Dropdown.Item as="button" onClick={selectWeight('> 10 кг', can, setCan)}>{'> 10 кг'}</Dropdown.Item>
                </Dropdown.Menu>
              </Dropdown>
            </div>
          </div>
        </div>
      </div>

      <div className='title'>
        Обрати час та мicце
      </div>

      <div style={{ padding: '20px' }}>
        <Map height={'200px'} onLocationChange={(location) => setLoc(location)} />
      </div>

      <div className='title'>
        Мої контакти
      </div>

      <div className='rectangle-input'>
        <input placeholder='Phone Number' onChange={inputChange(setPhoneNumber)} value={phoneNumber}/>
        <input placeholder='Email' onChange={inputChange(setEmail)} value={email}/>
      </div>

      <div className='title'>
        Комментар
      </div>

      <div className='rectangle-input'>
        <textarea placeholder='Написати комменентар' onChange={inputChange(setComment)} value={comment} style={{ width: '100%', height: '100%' }}/>
      </div>

      <div style={{ textAlign: 'center', padding: '20px 0px 20px 0px' }}>
        <Button variable="default" className='btn-declare' onClick={sendPropose}>
          Опублiковати
        </Button>
      </div>
    </div>
  );
}

const mapActionToProps = dispatch => ({ send: (...args) => dispatch(send(...args)) });

export default connect(null, mapActionToProps)(Declare);
