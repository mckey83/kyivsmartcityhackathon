import axios from 'axios';

import { LOADING_START, LOADING_FINISH, WASTED_POINTS } from '../types';

// const HOST = 'https://3.120.23.74:8080';
const HOST = 'https://3.120.23.74:8080';

export function loadingStart() {
  return { type: LOADING_START };
}

export function loadingFinish() {
  return { type: LOADING_FINISH };
}

export function send(location, units, comment) {
  return async dispatch => {

    dispatch(loadingStart);

    // await axios.post(`${HOST}/order`, {
    //   location: JSON.stringify(location),
    //   units,
    //   comment,
    // },
    // {
    //   headers: { common: { 'Access-Control-Allow-Origin': '*' } },
    // })
    Promise.resolve()
      .then(() => {
        dispatch(loadingFinish);
        alert('Дякуємо за вашу свідомість');
      })
      .catch(e => {
        dispatch(loadingFinish);
        console.log(e);
        alert('Error');
      });
  };
}

export function get(types = [1, 2, 3, 4]) {
  return async dispatch => {

    dispatch(loadingStart);

    await axios.get(`${HOST}/orders/${types.join(',')}`, {
      headers: { common: { 'Access-Control-Allow-Origin': '*' } },
    })
      .then((response) => {
        dispatch(loadingFinish);
        console.log(response.data);
        dispatch({
          type:    WASTED_POINTS,
          payload: response.data
            .filter(d => d.location && d.location !== 'loc')
            .map(d => ({
              id:       d.id,
              comment:  d.comment,
              location: JSON.parse(d.location),
              type:     d.wasteUnits[0].wasteType.id,
              title:    d.wasteUnits[0].wasteType.name,
              weight:   d.wasteUnits[0].weight,
            })),
        });
      })
      .catch(e => {
        dispatch(loadingFinish);
        console.log(e);
        alert('Error');
      });
  };
}

export function sendOrder(ids) {
  return async dispatch => {
    dispatch(loadingStart);

    // await axios.post(`${HOST}/orders/${ids.join(',')}`, {},
    //   {
    //     headers: { common: { 'Access-Control-Allow-Origin': '*' } },
    //   })
    Promise.resolve()
      .then(() => {
        dispatch(loadingFinish);
        alert('Дякуємо за вашу допомогу');
        dispatch({
          type:    WASTED_POINTS,
          payload: null,
        });
      })
      .catch(e => {
        dispatch(loadingFinish);
        console.log(e);
        alert('Error');
      });
  };
}
