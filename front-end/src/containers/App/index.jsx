import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { Portal } from 'react-portal';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import TurnOver from '../../pages/TurnOver';
import Declare from '../../pages/Declare';
import PickUp from '../../pages/PickUp';
import Main from '../../pages/Main';

import Spinner from '../../components/Spinner';

import 'bootstrap/dist/css/bootstrap.min.css';

function App(props) {
  
  return (
    <div style={{ backgroundColor: '#E9E9EA' }}>
      <Portal>
        {props.loading && <Spinner />}
      </Portal>
      <Router>
        <Switch>
          <Route exact path='/' component={Main} />
          <Route path='/turn-over/' component={TurnOver} />
          <Route path='/declare/' component={Declare} />
          <Route path='/pick-up/' component={PickUp} />
        </Switch>
      </Router>
    </div>
  );
}

App.propTypes = {
  loading: PropTypes.bool.isRequired,
};

function mapStateToProps({ appFlow }) {
  return {
    loading: appFlow.showSpinner,
  };
}

export default connect(mapStateToProps)(App);
