import React, { useState, useEffect } from 'react';
import { Button } from 'react-bootstrap';
import { Map, InfoWindow, Marker, GoogleApiWrapper, Polyline } from 'google-maps-react';

import './style.css';

function MyMap(props) {
  const [loc, setLoc] = useState({});
  const [info, setInfo] = useState({});
  const [targetLoc, setTargetLoc] = useState({});
  const [lines, setLines] = useState([]);
  const [isRout, setIsRout] = useState(false);

  useEffect(() => {
    if (!loc.latitude)
      navigator.geolocation.getCurrentPosition((position) => {
        setLoc(position.coords);
        if (props.onLocationChange) props.onLocationChange({ latitude: position.coords.latitude, longitude: position.coords.longitude });
      });
  });

  function onMarkerClick(props, marker, e) {
    setInfo({
      selectedPlace:     props,
      activeMarker:      marker,
      showingInfoWindow: true,
    });

    const latitude = e.latLng.lat();
    const longitude = e.latLng.lng();

    if (latitude !== loc.latitude && longitude !== loc.longitude) {
      // lines.push({ latitude, longitude });
      console.log(props);
      setTargetLoc({ latitude, longitude, id: props.unitId });
    }
  }

  function addPoint() {
    setLines([ ...lines, targetLoc ]);
    setTargetLoc({});
    setInfo({
      showingInfoWindow: false,
      activeMarker:      null,
    });

    if (props.onBuildRoute) {
      props.onBuildRoute(lines.map(({ id }) => id));
    }
  }

  function buildRout() {
    setIsRout(true);

    if (props.onBuildRoute) {
      props.onBuildRoute(lines.map(({ id }) => id));
    }
  }

  function onMapClicked (_props, map, e) {
    if (info.showingInfoWindow) {
      setInfo({
        showingInfoWindow: false,
        activeMarker:      null,
      });
      setLines([]);
      setTargetLoc({});
    } else {
      const latitude = e.latLng.lat();
      const longitude = e.latLng.lng();

      setLoc({ latitude, longitude });
      
      if (props.onLocationChange) props.onLocationChange({ latitude, longitude });
      
      setTargetLoc({});
      setLines([]);
    }
  }

  function makeMarkers(points = []) {
    return (points || []).map((point, ind) => {
      return (
        <Marker key={point.id || ind}
          unitId={point.id}
          title={point.title}
          name={point.name}
          weight={point.weight}
          position={point.loc}
          onClick={onMarkerClick}
          icon={{
            url:        point.icon,
            anchor:     new props.google.maps.Point(32, 32),
            scaledSize: new props.google.maps.Size(64, 64),
          }}
        />
      )
      ;
    });
  }

  return (
    <div className='map-div' style={{ height: props.height || '100%' }}>
      <Map
        google={props.google}
        className={'map'}
        style={{ width: props.width || '100%', height: props.isRout ? `calc(${props.height || '100%'} - 70px)` : props.height || '100%', position: 'relative' }}
        zoom={12}
        center={{ lat: loc.latitude || 0, lng: loc.longitude || 0 }}
        onClick={onMapClicked}
      >
 
        <Marker
          title={'Current location'}
          name={'Selected location'}
          position={{ lat: loc.latitude || 0, lng: loc.longitude || 0 }}
          onClick={onMarkerClick}
          icon={{
            url:        'img/current-loc.png',
            anchor:     new props.google.maps.Point(32, 32),
            scaledSize: new props.google.maps.Size(64, 64),
          }}
        />

        {makeMarkers(props.points)}

        {isRout && <Polyline
          path={[ { lat: loc.latitude, lng: loc.longitude }, ...lines.map(line => ({ lat: line.latitude, lng: line.longitude })) ]}
          strokeColor="#0000FF"
          strokeOpacity={0.8}
          strokeWeight={2} />}

        <InfoWindow
          marker={info.activeMarker}
          visible={info.showingInfoWindow}>
          <div>
            {targetLoc.latitude
              ? <div>
                <h6>Тип сміття: {(info.selectedPlace || {}).name}</h6>
                {(info.selectedPlace || {}).weight && <h6>Вага: {(info.selectedPlace || {}).weight}</h6>}
              </div>
              : <h6>Вибрана локація</h6>
            }
          </div>
        </InfoWindow>
      </Map>

      {props.isRout
        && <div style={{ display: 'inline-flex', width: '100%', height: '40px', paddingTop: '10px', position: 'absolute', bottom: '10px', left: 0 }}>
          <Button variant="primary" size="sm" disabled={!targetLoc.latitude} onClick={addPoint} style={{ width: '50%' }}>Додати точку</Button>
          <Button variant="success" size="sm" disabled={!lines.length} onClick={buildRout} style={{ width: '50%' }}>Побудувати маршрут</Button>
        </div>
      }
    </div>
  );
}

export default GoogleApiWrapper({
  apiKey: 'AIzaSyC7rOQ9UuzfozlUTVQVX3RS79OU6cbwwgo',
})(MyMap);
