import React from 'react';
import { ClipLoader } from 'react-spinners';
import { css } from '@emotion/core';

import './style.css';

const override = css`
    display: block;
    margin: 0 auto;
`;

function Spinner() {
  return (
    <div className='spinner spinner--fixed'>
      <ClipLoader
        css={override}
        sizeUnit={"px"}
        size={50}
        color={'#123abc'}
        loading={true}
      />
    </div>
  );
}

export default Spinner;
