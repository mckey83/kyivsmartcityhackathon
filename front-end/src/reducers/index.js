import { combineReducers } from 'redux';

import appFlowReducer from './appFlow';

export default combineReducers({
  appFlow: appFlowReducer,
});
