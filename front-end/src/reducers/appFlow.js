import { LOADING_START, LOADING_FINISH, WASTED_POINTS } from '../types';

const initState = {
  showSpinner: false,
  units:       null,
};

export default function rootReducer(state = initState, action) {
  switch (action.type) {
    case LOADING_START: {
      return { ...state, showSpinner: true };
    }
    case LOADING_FINISH: {
      return { ...state, showSpinner: false };
    }
    case WASTED_POINTS: {
      return { ...state, units: action.payload };
    }
    default: {
      return state;
    }
  }
}

