package org.smartcity.hackathon.wastesort.order;

import lombok.Data;
import org.smartcity.hackathon.wastesort.state.State;
import org.smartcity.hackathon.wastesort.wasteunit.WasteUnit;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
public class WasteOrder {
    @Id @GeneratedValue
    private Long id;

    private final Long supplierId;

    private Long consumerId;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    private final List<WasteUnit> wasteUnits = new ArrayList<>();

    private final String location;

    private final String comment;

    private final String time;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    private final List<State> states = new ArrayList<>();

    public WasteOrder(Long supplierId, String location, String comment, String time) {
        this.supplierId = supplierId;
        this.location = location;
        this.comment = comment;
        this.time = time;
    }

    protected WasteOrder() {
        this(null, null, null, null);
    }
}
