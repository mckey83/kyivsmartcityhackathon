package org.smartcity.hackathon.wastesort.order;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface WasteOrderRepository extends CrudRepository<WasteOrder, Long> {

    @Query(value = "SELECT DISTINCT * FROM WASTE_ORDER\n" +
            "  JOIN WASTE_ORDER_STATES j1 ON WASTE_ORDER.ID = j1.WASTE_ORDER_ID\n" +
            "  JOIN STATE state ON j1.STATES_ID = state.ID\n" +
            "  JOIN WASTE_ORDER_WASTE_UNITS j2 ON WASTE_ORDER.ID = j2.WASTE_ORDER_ID\n" +
            "  JOIN WASTE_UNIT u ON j2.WASTE_UNITS_ID = u.ID\n" +
            "WHERE state.NAME ='CREATED' AND u.WASTE_TYPE_ID IN(:wasteTypeIds);",
    nativeQuery = true)
    List<WasteOrder> getAllOpenWithFilterByWasteTypes(@Param("wasteTypeIds") List<Long> wasteTypeIds);

    List<WasteOrder> getWasteOrderBySupplierId(long consumerId);
}
