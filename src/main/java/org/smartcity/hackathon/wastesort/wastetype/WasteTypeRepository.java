package org.smartcity.hackathon.wastesort.wastetype;

import org.springframework.data.repository.CrudRepository;

public interface WasteTypeRepository extends CrudRepository<WasteType, Long> {
}
