package org.smartcity.hackathon.wastesort.collectionpoint;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.smartcity.hackathon.wastesort.wastetype.WasteType;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@RequiredArgsConstructor
public class CollectionPoint {
    @Id @GeneratedValue
    private Long id;

    private final String longtitude;

    private final String lattitude;

    @OneToMany(cascade = CascadeType.ALL)
    private final List<WasteType> wasteTypes = new ArrayList<>();

    protected CollectionPoint() {
        this(null, null);
    }
}
