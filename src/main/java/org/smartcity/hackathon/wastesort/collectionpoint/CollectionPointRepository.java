package org.smartcity.hackathon.wastesort.collectionpoint;

import org.springframework.data.repository.CrudRepository;

public interface CollectionPointRepository extends CrudRepository<CollectionPoint, Long> {
}
