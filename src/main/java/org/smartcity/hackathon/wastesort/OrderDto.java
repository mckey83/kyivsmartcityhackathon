package org.smartcity.hackathon.wastesort;

import lombok.Data;

import javax.persistence.Entity;
import java.util.List;

@Data
class OrderDto {
    Long supplierId;
    String location;
    String comment;
    String time;
    List<WasteUnitDto> units;
}
