package org.smartcity.hackathon.wastesort;

import lombok.Data;

@Data
public class WasteUnitDto {
    Long typeId;
    Long volume;
    Long weight;
}
