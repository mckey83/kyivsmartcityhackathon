package org.smartcity.hackathon.wastesort;

import lombok.extern.slf4j.Slf4j;
import org.smartcity.hackathon.wastesort.collectionpoint.CollectionPoint;
import org.smartcity.hackathon.wastesort.collectionpoint.CollectionPointRepository;
import org.smartcity.hackathon.wastesort.order.WasteOrder;
import org.smartcity.hackathon.wastesort.order.WasteOrderRepository;
import org.smartcity.hackathon.wastesort.state.State;
import org.smartcity.hackathon.wastesort.state.StateRepository;
import org.smartcity.hackathon.wastesort.wastetype.WasteType;
import org.smartcity.hackathon.wastesort.wastetype.WasteTypeRepository;
import org.smartcity.hackathon.wastesort.wasteunit.WasteUnit;
import org.smartcity.hackathon.wastesort.wasteunit.WasteUnitRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;

@Configuration
@EnableAutoConfiguration
@EnableJpaRepositories
@Slf4j
@SpringBootApplication
public class WastesortApplication {

	public static void main(String[] args) {
		SpringApplication.run(WastesortApplication.class, args);
	}

	@Autowired
    WasteTypeRepository wasteTypeRepository;
	@Autowired
    WasteUnitRepository wasteUnitRepository;
	@Autowired
    StateRepository stateRepository;
	@Autowired
    WasteOrderRepository orderRepository;
	@Autowired
    CollectionPointRepository collectionPointRepository;

    @PostConstruct
    void checkitOut() {
        WasteType paper = new WasteType("Paper");
		wasteTypeRepository.save(paper);
		WasteType glass = new WasteType("Glass");
		wasteTypeRepository.save(glass);
		WasteType plastic = new WasteType("Plastic");
		wasteTypeRepository.save(plastic);
		WasteType metal = new WasteType("Metal");
		wasteTypeRepository.save(metal);

        CollectionPoint collectionPoint = new CollectionPoint("30.508648", "50.466302");
        collectionPointRepository.save(collectionPoint);
        collectionPoint.getWasteTypes().add(paper);
        collectionPointRepository.save(collectionPoint);
    }

}
