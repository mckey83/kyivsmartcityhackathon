package org.smartcity.hackathon.wastesort;

import org.smartcity.hackathon.wastesort.order.WasteOrder;
import org.smartcity.hackathon.wastesort.order.WasteOrderRepository;
import org.smartcity.hackathon.wastesort.state.State;
import org.smartcity.hackathon.wastesort.state.StateRepository;
import org.smartcity.hackathon.wastesort.wastetype.WasteType;
import org.smartcity.hackathon.wastesort.wastetype.WasteTypeRepository;
import org.smartcity.hackathon.wastesort.wasteunit.WasteUnit;
import org.smartcity.hackathon.wastesort.wasteunit.WasteUnitRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import lombok.AllArgsConstructor;

@AllArgsConstructor
@RestController
@CrossOrigin
public class OrderSearchController {

    @Autowired
    WasteTypeRepository wasteTypeRepository;

    @Autowired
    WasteOrderRepository wasteOrderRepository;

    @Autowired
    WasteUnitRepository wasteUnitRepository;

    @Autowired
    StateRepository stateRepository;


    @GetMapping("/orders/{wasteTypeIds}")
    List<WasteOrder> searchAllOpenWithFilterByWasteTypes(@PathVariable List<Long> wasteTypeIds) {
       return wasteOrderRepository.getAllOpenWithFilterByWasteTypes(wasteTypeIds);
    }

    @GetMapping("/orders/supplier/{supplierId}")
    List<WasteOrder> getWasteOrderBySupplierId(@PathVariable("supplierId") long consumerId) {
        return wasteOrderRepository.getWasteOrderBySupplierId(consumerId);
    }

    @PostMapping("/orders/{consumerId}/{wasteOrdersIds}")
    void saveConsumerWasteOrdersPoint(@PathVariable("consumerId") long consumerId, @PathVariable("wasteOrdersIds") List<Long> wasteOrdersIds) {
        wasteOrderRepository.findAllById(wasteOrdersIds)
                            .forEach(wasteOrder -> {
                                State stateInProgress = new State("IN_PROGRESS", new Date());
                                stateRepository.save(stateInProgress);
                                wasteOrder.getStates().add(stateInProgress);
                                wasteOrder.setConsumerId(consumerId);
                                wasteOrderRepository.save(wasteOrder);
                            });
    }

    @GetMapping("/waste-types")
    List<WasteType> getWasteTypes() {
        ArrayList<WasteType> wasteTypes = new ArrayList<>();
        wasteTypeRepository.findAll().forEach(wasteTypes::add);
        return wasteTypes;
    }

    @PostMapping("/order")
    WasteOrder createOrder(@RequestBody OrderDto orderDto) {
        WasteOrder wasteOrder = new WasteOrder(orderDto.getSupplierId(), orderDto.getLocation(), orderDto.getComment(), orderDto.getTime());
        wasteOrderRepository.save(wasteOrder);
        wasteOrder.getStates().add(new State("CREATED", new Date()));
        for (WasteUnitDto unit : orderDto.units) {
            wasteTypeRepository.findById(unit.getTypeId())
                               .map(wasteType -> wasteOrder.getWasteUnits().add(new WasteUnit(wasteType, unit.getVolume(), unit.getWeight())))
                               .orElseThrow(RuntimeException::new);
        }
        wasteOrderRepository.save(wasteOrder);

        return wasteOrder;
    }
}
