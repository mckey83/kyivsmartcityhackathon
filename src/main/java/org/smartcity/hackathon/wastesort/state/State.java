package org.smartcity.hackathon.wastesort.state;

import java.util.Date;

import lombok.Data;
import lombok.RequiredArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
@Data
@RequiredArgsConstructor
public class State {
    @Id @GeneratedValue
    private Long id;

    private final String name;

    private final Date time;

    protected State() {
        this(null, null);
    }
}
