package org.smartcity.hackathon.wastesort.state;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

public interface StateRepository extends CrudRepository<State, Long> {
}
