package org.smartcity.hackathon.wastesort.wasteunit;

import org.smartcity.hackathon.wastesort.wastetype.WasteType;
import org.springframework.data.repository.CrudRepository;

public interface WasteUnitRepository extends CrudRepository<WasteUnit, Long> {
}
