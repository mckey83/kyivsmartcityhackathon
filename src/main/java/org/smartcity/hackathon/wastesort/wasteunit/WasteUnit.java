package org.smartcity.hackathon.wastesort.wasteunit;

import lombok.*;
import org.smartcity.hackathon.wastesort.wastetype.WasteType;

import javax.persistence.*;

@Entity
@Data
@RequiredArgsConstructor
public class WasteUnit {

    @Id @GeneratedValue
    Long id;

    @OneToOne
    private final WasteType wasteType;

    private final Long volume;

    private final Long weight;

    protected WasteUnit() {
        this(null, null, null);
    }

}
