package org.smartcity.hackathon.wastesort

import com.fasterxml.jackson.databind.ObjectMapper
import org.smartcity.hackathon.wastesort.order.WasteOrder
import org.smartcity.hackathon.wastesort.order.WasteOrderRepository
import org.smartcity.hackathon.wastesort.state.StateRepository
import org.smartcity.hackathon.wastesort.wastetype.WasteType
import org.smartcity.hackathon.wastesort.wastetype.WasteTypeRepository
import org.smartcity.hackathon.wastesort.wasteunit.WasteUnitRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import spock.lang.Specification
import spock.lang.Unroll

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath

class OrderSearchControllerTest extends Specification {

    WasteTypeRepository wasteTypeRepository = Mock()

    WasteOrderRepository wasteOrderRepository = Mock()

    WasteUnitRepository wasteUnitRepository = Mock()

    StateRepository stateRepository  = Mock()

    def controller = new OrderSearchController(wasteTypeRepository, wasteOrderRepository, wasteUnitRepository, stateRepository)

    MockMvc mockMvc = standaloneSetup(controller).build()

    @Autowired
    ObjectMapper objectMapper

    @Unroll
    def "A Consumer can get all new Waste Orders"() {
        given:
        wasteOrderRepository.getAllOpenWithFilterByWasteTypes(_) >> [new WasteOrder(id: 1)]

        when:
        def response = mockMvc.perform(MockMvcRequestBuilders.get('/orders/1,2'))

        then:
        response.andExpect(content().string('[{"id":1,"supplierId":null,"consumerId":null,"wasteUnits":[],"location":null,"comment":null,"time":null,"states":[]}]'))
    }

    @Unroll
    def "A Consumer can get own saved Waste Orders for path"() {
        given:
        wasteOrderRepository.getWasteOrderBySupplierId(_) >> [new WasteOrder(id: 1)]

        when:
        def response = mockMvc.perform(MockMvcRequestBuilders.get('/orders/supplier/1'))

        then:
        response.andExpect(content().string('[{"id":1,"supplierId":null,"consumerId":null,"wasteUnits":[],"location":null,"comment":null,"time":null,"states":[]}]'))
    }

    @Unroll
    def "A Supplier can save chosen Waste Orders for path"() {
        given:
        wasteOrderRepository.findAllById(_) >> [new WasteOrder(id: 1)]

        when:
        mockMvc.perform(MockMvcRequestBuilders.post('/orders/1/1'))

        then:
        1 * stateRepository.save(_)
        1 * wasteOrderRepository.save(_);
    }

    @Unroll
    def "GetWasteTypes"() {
        given:
        wasteTypeRepository.findAll() >> [new WasteType("GLASS"), new WasteType("PAPER")]

        when:
        def response = mockMvc.perform(MockMvcRequestBuilders.get('/waste-types'))

        then:
        response.andExpect(content().string('[{"id":null,"name":"GLASS"},{"id":null,"name":"PAPER"}]'))
    }

    @Unroll
    def "A Supplier can create new Waste Order"() {
        given:
        def units = [new WasteUnitDto(typeId: 1L, volume: 10L, weight:5L)]

        and:
        def dto = new OrderDto(supplierId: 1, location: "my location", comment: "my comment", time: "today", units: units)

        and:
        wasteTypeRepository.findById(_) >> Optional.ofNullable(new WasteType("GLASS"))

        when:
        mockMvc.perform(MockMvcRequestBuilders.post('/order')
                                              .contentType(MediaType.APPLICATION_JSON)
                                              .content(new ObjectMapper().writeValueAsString(dto)))
        then:
        2 * wasteOrderRepository.save(_)
    }
}
